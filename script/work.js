// {
// 	"employment": [{
// 		"name": "JSC 'Instrument-making plant Optron'",
// 		"date": ["11 2015", "01 2016"]
// 	}, {
// 		"name": "PUE 'EngMechLogist'",
// 		"date": ["01 2016", "04 2017"]
// 	}, {
// 		"name": "Unitary Enterprise 'AES Komplekt'",
// 		"date": ["04 2017", "Present"]
// 	}]
// }


let svg = d3.select('.work').append('svg').attr('class', 'infographic').attr('id', 'exp_infographic');
const height = parseInt(getComputedStyle(document.getElementById('exp_infographic')).height);
const width = parseInt(getComputedStyle(document.getElementById('exp_infographic')).width);
const padding = 20;

d3.json("https://raw.githubusercontent.com/Leamhein/Json/2f431d366bce1d9538959472545e7c75dc46aa5a/data.json").then(function (data) {

	let startDate = data.employment[0].date[0].split(" ");
	startDate = new Date(+startDate[1], +startDate[0] - 1);
	let today = new Date();
	//масштаб
	let xScale = d3.scaleTime().domain([startDate, today]).rangeRound([padding, width - padding]);
	//оси
	let xAxis = d3.axisBottom(xScale);
	//отрисовка оси
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0," + (height - padding) + ")")
		.call(xAxis);
	//.attr("transform", "translate(0," + (height - padding) + ")") - смещает ось вниз страницы
	//.ticks(3) - желаемое кол-во меток на оси
	var bar = svg.selectAll(".bar")
		.data(data.employment)
		.enter().append("g")
		.attr("class", "bar")
		.attr("transform", function (d) {
			let width = widthCalc(d);
			let heightG = (height - padding - 105);
			let str = "translate(" + width + "," + heightG + ")"
		});

	bar.append("rect")
		.attr("x", (d) => {
			let date = d.date[0].split(" ");
			date = new Date(+date[1], +date[0] - 1);
			return xScale(date);
		})
		.attr("width", (d) => {
			return widthCalc(d)
		})
		.attr("height", 50).attr("transform", "translate(0," + (height - padding - 55) + ")").attr("fill", (d, i) => {
			return colores_google(i)
		});

	bar.append('title').text((d) => {
		let dateEmp = d.date[0] + ' - ' + d.date[1];
		return d.name + ' ' + dateEmp + "";
	})

	function widthCalc(d) {
		let startDate = d.date[0].split(" ");
		startDate = new Date(+startDate[1], +startDate[0] - 1);

		let endDate;
		if (d.date[1] == 'Present') {
			endDate = new Date();
		} else {
			endDate = d.date[1].split(" ");
			endDate = new Date(+endDate[1], +endDate[0] - 1);
		}
		return xScale(endDate) - xScale(startDate);
	}

	function colores_google(n) {
		var colores_g = ["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477", "#66aa00", "#b82e2e", "#316395", "#994499", "#22aa99", "#aaaa11", "#6633cc", "#e67300", "#8b0707", "#651067", "#329262", "#5574a6", "#3b3eac"];
		return colores_g[n % colores_g.length];
	}
});
