// "skills": [
//     {
//       "tech": "Web Development",
//       "skills": [
//         "CoreJS",
//         "React",
//         "Redux",
//         "D3.js"
//       ]
//     },
//     {
//       "tech": "UI/UX Design",
//       "skills": [
//         "Photoshop",
//         "Illustrator"
//       ]
//     },
//     {
//       "tech": "Frontend",
//       "skills": [
//         "HTML",
//         "CSS"
//       ]
//     }
//   ]

let svgSkills = d3.select('.skills').append('svg').attr('class', 'infographic').attr('id', 'exp_infographic');
let heightSkills = parseInt(getComputedStyle(document.getElementById('exp_infographic')).height);
let widthSkills = parseInt(getComputedStyle(document.getElementById('exp_infographic')).width);
const paddingSkills = 20;
let tech = d3.scaleLinear()
    .domain([paddingSkills, widthSkills/2, widthSkills-paddingSkills])
    .range(["Web Development", "UI/UX Design", "Frontend"]);
console.log(tech("Web Development"));


d3.json("https://raw.githubusercontent.com/Leamhein/Json/2f431d366bce1d9538959472545e7c75dc46aa5a/data.json").then((data)=>{
    let circles = svgSkills.selectAll('.circles')
    .data(data.skills).enter().append("g").attr('class', 'circle').attr("transform", function (d) {
        let width = 100;
        let heightG = (heightSkills - paddingSkills - 105);
        let str = "translate(" + width + "," + heightG + ")"
    });

    circles.append('circle').attr("r", (heightSkills-paddingSkills)/2).attr('cx', 90)
    


	function colores_google(n) {
		var colores_g = ["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477", "#66aa00", "#b82e2e", "#316395", "#994499", "#22aa99", "#aaaa11", "#6633cc", "#e67300", "#8b0707", "#651067", "#329262", "#5574a6", "#3b3eac"];
		return colores_g[n % colores_g.length];
	}
});